JustBuild
=========

JustBuild is a multi-platform tool in java, which allows for easy automation
 of building process for virtually any technology- currently in very early pre-pre
alpha stage and also a hooby projects, so it only supports building C/C++ projects with
g++ on linux "out-of-the-box", also more an example than as production-ready builder, 
but worry not, basic core functionality is already almost in place, and with its extreme 
extensibility JustBuild already allows you to write you own builders,
providing a possibility to compile, link or do whatever else you need with your sources
written in virtually any language, to build executables, libraries or whatever else you
need on whatever system (as long as it runs Java). And thats only a two-sentence 
description ;)
