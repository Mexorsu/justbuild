package edu.szyrek.jb.exception;

public class CausedException extends Exception {
	Throwable cause;
	public CausedException(Throwable cause) {
		this.cause = cause;
	}
	public CausedException(String message) {
		super(message);
		this.cause= null;
	}
}
