package edu.szyrek.jb.exception;

public class ModulesDiscoveryException extends CausedException {

	private static final long serialVersionUID = -5129090522182744737L;

	public ModulesDiscoveryException(Throwable cause) {
		super(cause);
	}

}
