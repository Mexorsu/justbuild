package edu.szyrek.jb.input;

import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.dependency.faxus.FaxusDependency;

public class FaxusDynamicLibInput extends DependencyInput {
	public FaxusDynamicLibInput(String name, String version, String compiler, String stdLib, boolean debug) {
		this.value =  FaxusDependency.newDynamicLib(name, version, compiler, stdLib, debug);
	}
}
