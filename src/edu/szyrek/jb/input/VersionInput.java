package edu.szyrek.jb.input;

import edu.szyrek.jb.BuildPhase;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.api.UniqueInput;

public class VersionInput extends UniqueInput<String> {
	public static final String VERSION_SEPARATOR = ".";
	public static final String VERSION_SEPARATOR_REGEX = "\\.";
	
	
	public VersionInput(String version){
		this.value = version;
	}
	public VersionInput(int major, int minor, Integer release){
		this.value = major+VERSION_SEPARATOR+minor+(release == null ? "": VERSION_SEPARATOR+release);
	}
	public int getMajor() {
		return Integer.parseInt(this.value.split(VERSION_SEPARATOR_REGEX)[0]);
	}
	public int getMinor() {
		return Integer.parseInt(this.value.split(VERSION_SEPARATOR_REGEX)[1]);
	}
	public Integer getRelease() {
		if (this.value.split(VERSION_SEPARATOR_REGEX).length>=3){
			return Integer.parseInt(this.value.split(VERSION_SEPARATOR_REGEX)[2]);
		} else {
			return null;
		}
	}
}
