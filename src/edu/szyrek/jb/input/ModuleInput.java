package edu.szyrek.jb.input;
import edu.szyrek.jb.dependency.bb.Module;

public class ModuleInput extends DependencyInput {
	public ModuleInput(String name, String version) {
		this.value = new Module(name, version);
	}
}
