package edu.szyrek.jb.input;

import edu.szyrek.jb.JBTarget;
import edu.szyrek.jb.api.Target;
import edu.szyrek.jb.api.UniqueInput;

public class TargetInput extends UniqueInput<Target> {
	
	public TargetInput() {
		this.value= JBTarget.NOT_SET;
	}
	
	public TargetInput(Target target) {
		this.value = target;
	}
	
}
