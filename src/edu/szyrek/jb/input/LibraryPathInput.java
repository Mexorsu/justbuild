package edu.szyrek.jb.input;

import java.io.IOException;

import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.util.Directory;

public class LibraryPathInput extends BaseInput<Directory> {
	
	public LibraryPathInput(String filePath) throws IOException {
		this.value = Directory.open(filePath);
	}
	
}
