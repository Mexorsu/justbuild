package edu.szyrek.jb.input;

import edu.szyrek.jb.api.BaseInput;

public class CustomFlagInput extends BaseInput<String>{
	public enum Scope {
		COMPILE, LINKER, BOTH
	}
	protected Scope scope = Scope.BOTH;
	public CustomFlagInput(String s) {
		this.value = s;
	}
	public CustomFlagInput(String s, Scope scope) {
		this.scope = scope;
	}
}
