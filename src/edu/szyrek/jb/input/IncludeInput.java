package edu.szyrek.jb.input;

import java.io.FileNotFoundException;
import java.nio.file.NotDirectoryException;

import edu.szyrek.jb.BuildPhase;
import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.util.Directory;

public class IncludeInput extends BaseInput<Directory> {
	public IncludeInput(String path) throws NotDirectoryException, FileNotFoundException {
		this.value = Directory.open(path);
	}


}
