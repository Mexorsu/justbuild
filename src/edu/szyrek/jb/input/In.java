package edu.szyrek.jb.input;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface In {
    Class<? extends edu.szyrek.jb.api.Input<?>> type();
    boolean collection() default false;
}
