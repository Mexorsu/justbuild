package edu.szyrek.jb.input;

public class CompileFlagInput extends CustomFlagInput {

	public CompileFlagInput(String s) {
		super(s);
		this.scope = Scope.COMPILE;
	}
}
