package edu.szyrek.jb.input;

import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.dependency.Artifact;

public class ArtifactInput extends BaseInput<Artifact> {
	public ArtifactInput(Artifact art) {
		this.value = art;
	}
}
