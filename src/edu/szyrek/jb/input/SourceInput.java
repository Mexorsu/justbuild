package edu.szyrek.jb.input;

import java.io.FileNotFoundException;

import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.util.File;

public class SourceInput extends BaseInput<File> {
	public SourceInput(String value) throws FileNotFoundException{
		this.value = File.open(value);
		if (!this.value.exists()){
			throw new IllegalArgumentException(value+ " is not a file");
		}
	}
}
