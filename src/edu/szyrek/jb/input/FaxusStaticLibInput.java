package edu.szyrek.jb.input;

import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.dependency.faxus.FaxusDependency;

public class FaxusStaticLibInput extends DependencyInput {
	public FaxusStaticLibInput(String name, String version, String compiler, boolean debug) {
		this.value =  FaxusDependency.newStaticLib(name, version, compiler, debug);
	}
}
