package edu.szyrek.jb.input;

import java.io.FileNotFoundException;

import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.util.File;

public class ExportHeaderInput extends BaseInput<File>{
	public ExportHeaderInput(String path) throws FileNotFoundException {
		this.value = File.open(path);
	}
}
