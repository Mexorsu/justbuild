package edu.szyrek.jb.input;

import edu.szyrek.jb.input.CustomFlagInput.Scope;

public class LinkFlagInput extends CustomFlagInput{
	public LinkFlagInput(String s) {
		super(s);
		this.scope = Scope.LINKER;
	}
}
