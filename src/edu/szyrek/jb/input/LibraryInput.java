package edu.szyrek.jb.input;

import edu.szyrek.jb.api.BaseInput;

public class LibraryInput extends BaseInput<String>{
	
	public LibraryInput(String name) {
		this.value = name;
	}
	
}
