package edu.szyrek.jb.input;

import java.io.IOException;
import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.util.File;

public class ObjectFileInput extends BaseInput<File>{
	public ObjectFileInput(String filePath) throws IOException {
		this.value = File.createOrOpen(filePath);
	}
}
