package edu.szyrek.jb.input;

import edu.szyrek.jb.api.UniqueInput;

public class AppNameInput extends UniqueInput<String>{
	
	public AppNameInput() {
		this.value = "Untitled";
	}
	
	public AppNameInput(String name) {
		this.value = name.replace(' ', '_');
	}
}
