package edu.szyrek.jb.input;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import edu.szyrek.jb.api.UniqueInput;
import edu.szyrek.jb.util.Directory;

public class OutDir extends UniqueInput<Directory> {
	public OutDir(String s) throws FileAlreadyExistsException, NotDirectoryException {
		try {
			this.value = Directory.open(s);
		} catch (NotDirectoryException|FileNotFoundException e) {
			this.value = Directory.create(s);
		}
	}
}
