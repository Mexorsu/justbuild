package edu.szyrek.jb.api;

public interface InputReceiver<T> {
    public void receive(T input);
}
