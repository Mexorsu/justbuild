package edu.szyrek.jb.api;

import java.util.Objects;

import edu.szyrek.jb.BuildPhase;

public abstract class UniqueInput<T> implements Input<T> {
	protected T value;
	
	@Override
	public final int getInputID() {
		return 0;
	}

	@Override
	public final boolean equals(Object other) {
		if (other!=null&&this.getClass().isAssignableFrom(other.getClass())){
			return true;
		}return false;
	}
	@Override
	public final int hashCode() {
		return Objects.hash(this.getClass().getName());
	}
	

	@Override
	public T getValue() {
		return this.value;
	}
	@Override
	public void setValue(T val) {
		this.value = val;
	}

	@Override
    public boolean doesTarget(BuildPhase phase){
        return true;
    }

	@Override
	public String toString() {
		return this.value.toString();
	}
	@Override
	public String getDescription() {
		return this.getClass().getSimpleName()+": ["+this.value.toString()+"]";
	}
	
}
