package edu.szyrek.jb.api;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import edu.szyrek.jb.BuildPhase;
import edu.szyrek.jb.dependency.bb.BBDependenciesResolvingPhase;
import edu.szyrek.jb.dependency.bb.BBDependencyFetcher;
import edu.szyrek.jb.dependency.faxus.FaxusDependenciesResolvingPhase;
import edu.szyrek.jb.exception.BuildFailedException;
import edu.szyrek.jb.input.In;
import edu.szyrek.jb.input.Out;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.DoubleLinkedList;


public abstract class Builder implements CommandsGenerator {
	protected Queue<Command> commands = new LinkedList<>();
	protected DoubleLinkedList<BuildPhase> phases = new DoubleLinkedList<>();
	protected Map<BuildPhase,Set<Input<?>>> inputs = new HashMap<>();
	protected Settings settings;
	protected Map<Integer, Boolean> isSet = new HashMap<>();
	protected Set<Input<?>> artifacts = new HashSet<>();
	
	public Set<Input<?>> getArtifacts() {
		return artifacts;
	}

	public void addArtifact(Input<?> artifact) {
		this.artifacts.add(artifact);
	}

	public Builder() {}
	
	public abstract void initialize();
	
	public final void config(Settings settings) {
		this.settings = settings;
	}
	
    public void mapCollection(Field f, Class<? extends Input<?>> type, BuildPhase instance) {
        Set<Input<?>> values = new HashSet<>();
        for (Input<?> input: inputs.get(instance)) {
            if (input.doesTarget(instance)  && type.isAssignableFrom(input.getClass())){
                values.add(input);
            }
        }
        try {
        	f.setAccessible(true);
            @SuppressWarnings("unchecked")
			Collection<Input<?>> instanceValue = (Collection<Input<?>>) f.get(instance);
            if (instanceValue==null) {
            	instanceValue = new HashSet<>();
            }
            instanceValue.addAll(values);
            f.set(instance, instanceValue);
            f.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void mapInput(Field f, Class<? extends Input<?>> type, BuildPhase instance) {
        Input<?> value = null;
        for (Input<?> input: inputs.get(instance)) {
            if (input.doesTarget(instance)  && type.isAssignableFrom(input.getClass())){
                value = input;
                break;
            }
        }
        try {
        	f.setAccessible(true);
            f.set(instance, value);
            f.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mapOutput(Field f, Class<? extends Input<?>> type, BuildPhase instance) {
        try {
			Input<?> value = (Input<?>) f.get(instance);
			
			for (BuildPhase phase: phases) {
				if (value.doesTarget(phase)){
					if (inputs.get(phase)==null){
						inputs.put(phase, new HashSet<Input<?>>());
					}
					inputs.get(phase).add(value);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
    }

    public void mapOutCollection(Field f, Class<? extends Input<?>> type, BuildPhase instance) {
        try {
			@SuppressWarnings("unchecked")
			Collection<Input<?>> values = (Collection<Input<?>>) f.get(instance);
			if (values==null || values.size()==0) return;
			Input<?> typeInstance = null;
			for (Input<?> val: values){
				typeInstance = val;
				break;
			}
			for (BuildPhase phase: phases) {
				if (typeInstance.doesTarget(phase)){
					if (inputs.get(phase)==null){
						inputs.put(phase, new HashSet<Input<?>>());
					}
					inputs.get(phase).addAll(values);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
    }
	public void generate(){
		for (BuildPhase phase: phases) {
			for (Input<?> in: settings.getInputs()) {
				if (!in.doesTarget(phase)) continue;
				if (inputs.get(phase)==null) {
					this.inputs.put(phase, new HashSet<>());
				}
				this.inputs.get(phase).add(in);
			}
		}
		while (!phases.isEmpty()){
			// Map @In s for phase
            BuildPhase phase;
            if ((phase = phases.poll())!=null){
	            for (Field f: phase.getClass().getDeclaredFields()){
	                for (Annotation a: f.getDeclaredAnnotations()) {
	                    if (a instanceof In) {
	                        In in = (In) a;
	                        Class<? extends Input<?>> inputType = in.type();
	                        if (in.collection()==true){
	                            mapCollection(f, inputType, phase);                
	                        }else{
	                        	mapInput(f, inputType, phase);
	                        }
	                    }
	                }
	            }
	            commands.addAll(phase.execute());
	            for (Field f: phase.getClass().getDeclaredFields()){
	                for (Annotation a: f.getDeclaredAnnotations()) {
	                    if (a instanceof Out) {
	                    	Out out = (Out) a;
	                        Class<? extends Input<?>> inputType = out.type();
	                        if (out.collection()==true){
	                            mapOutCollection(f, inputType, phase);                
	                        }else{
	                        	mapOutput(f, inputType, phase);
	                        }
	                    }
	                }
	            }
            }
		}
	}
	
	private void checkUnique(BuildPhase phase) {
		if (this.phases.contains(phase)) {
			throw new IllegalArgumentException("Phase " + phase+ " already registered");
		}
	}
	
	public void addPhase(BuildPhase phase) {
		checkUnique(phase);
		this.phases.addLast(phase);
	}
	
	public void addPhaseBefore(BuildPhase phase, BuildPhase anchor) {
		checkUnique(phase);
		this.phases.addBefore(phase, anchor);
	}
	
	public void addPhaseAfter(BuildPhase phase, BuildPhase anchor) {
		checkUnique(phase);
		this.phases.addAfter(phase, anchor);
	}
	public void pushCmd(Command cmd) {
		this.commands.offer(cmd);
	}

	public Map<BuildPhase,Set<Input<?>>> getInputs() {
		return this.inputs;
	}
	
	@Override
	public Queue<Command> getCommands() {
		return this.commands;
	}

}
