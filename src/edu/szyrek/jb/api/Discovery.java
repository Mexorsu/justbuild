package edu.szyrek.jb.api;

import java.util.List;

import edu.szyrek.jb.util.Described;
import edu.szyrek.jb.util.Joiner;
import edu.szyrek.jb.util.Path;

public interface Discovery extends Described {
	public final static int DEFAULT_DEPTH = 256;
	public List<String> includes();
	public List<String> excludes();
	public Input<?> pathToInput(Path path);
	
	@Override
	default String getDescription(){
		StringBuilder sb = new StringBuilder();
			sb.append("Discovery: [");
			sb.append(" includes: {"+ Joiner.join(includes())+"},");
			sb.append(" excludes: {"+ Joiner.join(excludes())+"}");
			sb.append("]");
			return sb.toString(); 
	      
    }
}
