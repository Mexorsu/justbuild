package edu.szyrek.jb.api;

import edu.szyrek.jb.BuildPhase;
import edu.szyrek.jb.util.Described;

public interface Input<T> extends Described {
	public T getValue();
	public void setValue(T val);
    public boolean doesTarget(BuildPhase phase);
    public int getInputID();
}
