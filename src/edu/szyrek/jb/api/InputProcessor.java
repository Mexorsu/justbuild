package edu.szyrek.jb.api;

import java.util.Collection;

public interface InputProcessor<T extends Input<?>> {
    public void process(Collection<T> inputs);
}
