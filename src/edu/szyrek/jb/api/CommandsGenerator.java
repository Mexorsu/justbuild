package edu.szyrek.jb.api;
import java.util.Queue;

public interface CommandsGenerator {
	public void config(Settings settings);
	public void generate();
	public Queue<Command> getCommands();
}
