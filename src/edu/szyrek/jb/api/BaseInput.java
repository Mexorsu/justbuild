package edu.szyrek.jb.api;

import java.util.Objects;

import edu.szyrek.jb.BuildPhase;

public abstract class BaseInput<T> implements Input<T> {
	protected T value;
	protected static int nextInputID = 0;
	private int inputID = getNextInputID();
	
	protected static int getNextInputID() {
		return nextInputID++;
	}
	
	@Override
	public final int getInputID() {
		return inputID;
	}

	@Override
	public boolean equals(Object other) {
		if (other!=null&&this.getClass().isAssignableFrom(other.getClass())
				&& this.getInputID()==((Input)other).getInputID()){
			return true;
		}return false;
	}
	@Override
	public int hashCode() {
		return Objects.hash(31*this.inputID);
	}
	
	@Override
	public T getValue() {
		return this.value;
	}
	@Override
	public void setValue(T val) {
		this.value = val;
	}

	@Override
    public boolean doesTarget(BuildPhase phase){
        return true;
    }

	@Override
	public String toString() {
		return this.value.toString();
	}
	@Override
	public String getDescription() {
		return this.getClass().getSimpleName()+": ["+this.value.toString()+"]";
	}
	
}
