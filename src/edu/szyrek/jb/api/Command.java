package edu.szyrek.jb.api;

import edu.szyrek.jb.Result;
import edu.szyrek.jb.exception.CommandFailedException;

public interface Command {
	public Result run() throws CommandFailedException;
}
