package edu.szyrek.jb.api;
import java.io.IOException;
import java.util.Set;

import edu.szyrek.jb.Result;
import edu.szyrek.jb.util.Directory;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public interface Settings {
    
    public abstract Result validateSettings();
        
    public abstract Set<Input<?>> getInputs();
    
    public abstract String getBuilder();
    
    public abstract Directory getBuildRoot();

	public abstract void initialize() throws ResourceException, ScriptException, IOException, InstantiationException, IllegalAccessException;
    
}
