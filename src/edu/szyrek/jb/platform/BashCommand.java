package edu.szyrek.jb.platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import edu.szyrek.jb.Result;
import edu.szyrek.jb.api.Command;
import edu.szyrek.jb.exception.CommandFailedException;

public class BashCommand implements Command {
	String commandString;
	
	public BashCommand(String cmd) {
		this.commandString = cmd;
	}
	
	@Override
	public String toString() {
		return this.commandString;
	}
	
	@Override
	public Result run() throws CommandFailedException {
		Result res = new Result();
		try {
			Runtime run = Runtime.getRuntime();
			Process pr = run.exec(commandString);
			int houstonWeveGotAProblem = pr.waitFor();
			BufferedReader out = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			BufferedReader err = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			StringBuilder errorString = new StringBuilder();
			
			for( String line = line = out.readLine() ; line != null ;line = out.readLine()) 
			{
				res.appendOut(line+"\n");
			}
			if (houstonWeveGotAProblem!=0) {
				for( String line = err.readLine() ; line != null ;line = err.readLine()) 
				{
					errorString.append("ERROR: "+line+"\n");
				}
			}else{
				for( String line = err.readLine() ; line != null ;line = err.readLine()) 
				{
					res.appendOut("WARN: "+line+"\n");
				}	
			}

			if (errorString.length()>0){
				res.addError(errorString.toString());
			}
			return res;
		}catch (IOException e) {
			e.printStackTrace();
			throw new CommandFailedException();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new CommandFailedException();
		}
	}

}
