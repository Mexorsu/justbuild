package edu.szyrek.jb.dependency;

import java.util.Collection;

import edu.szyrek.jb.api.Input;

public abstract class DependencyResolver {
	public abstract Collection<Input<?>> resolve(Dependency dep);
}
