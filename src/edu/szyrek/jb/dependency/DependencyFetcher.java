package edu.szyrek.jb.dependency;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;

import edu.szyrek.faxus.filesystem.NoSuchDependencyException;
import edu.szyrek.jb.Result;
import edu.szyrek.jb.util.Directory;

public abstract class DependencyFetcher {

	public abstract Directory fetch(Dependency dependency) throws NotDirectoryException, FileNotFoundException, FileAlreadyExistsException, IOException, URISyntaxException, NoSuchDependencyException;

}
