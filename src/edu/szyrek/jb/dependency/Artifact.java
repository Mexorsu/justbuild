package edu.szyrek.jb.dependency;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.szyrek.jb.api.Input;

public class Artifact {
	private String name;
	private String version;
	private Set<Input<?>> outputs = new HashSet<>();
	
	public Artifact(String name, String version) {
		super();
		this.name = name;
		this.version = version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Set<Input<?>> getOutputs() {
		return outputs;
	}

	public void append(Input<?> in) {
		this.outputs.add(in);
	}
	
	public void appendAll(Collection<Input<?>> ins) {
		this.outputs.addAll(ins);
	}
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Artifact)){
			return false;
		}
		Artifact other = (Artifact) o;
		return this.name.equals(other.getName())&&this.version.equals(other.getVersion());
	}
	
}
