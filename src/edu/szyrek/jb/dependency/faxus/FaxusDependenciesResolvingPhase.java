package edu.szyrek.jb.dependency.faxus;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import edu.szyrek.jb.BuildPhase;
import edu.szyrek.jb.api.BaseInput;
import edu.szyrek.jb.api.Command;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetchException;
import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.exception.DependencyResolveException;
import edu.szyrek.jb.input.DependencyInput;
import edu.szyrek.jb.input.In;
import edu.szyrek.jb.input.IncludeInput;
import edu.szyrek.jb.input.LibraryInput;
import edu.szyrek.jb.input.LibraryPathInput;
import edu.szyrek.jb.input.Out;
import edu.szyrek.jb.util.Directory;

public class FaxusDependenciesResolvingPhase extends BuildPhase {
	@In(type=DependencyInput.class, collection=true)
	public Set<DependencyInput> dependencies = new HashSet<>();
	@Out(type=LibraryInput.class, collection=true)
	public Set<LibraryInput> libOutputs = new HashSet<>();
	@Out(type=LibraryPathInput.class, collection=true)
	public Set<LibraryPathInput> libPathOutputs = new HashSet<>();
	@Out(type=IncludeInput.class, collection=true)
	public Set<IncludeInput> includeOutputs = new HashSet<>();
	
	@Override
	public Queue<Command> execute() {
		FaxusDependencyFetcher fetcher = new FaxusDependencyFetcher("127.0.0.1", 5217);
			for (DependencyInput dependency: dependencies) {
				try {
					Dependency dep = dependency.getValue();
					if (dep.isResolved()||!(dep instanceof FaxusDependency)){
						continue;
					}
					System.out.println("Resolving dependency: "+dependency.toString());
					Directory dir = dep.fetch(fetcher);
					Set<Input> res = new HashSet<>();
					res.addAll(dep.resolve(dir));
					for (Input in: res) {
						if (in instanceof LibraryInput) {
							libOutputs.add((LibraryInput)in);
						} else if (in instanceof LibraryPathInput) {
							libPathOutputs.add((LibraryPathInput)in);
						}  else if (in instanceof IncludeInput) {
							includeOutputs.add((IncludeInput)in);
						}
					}
					dep.setResolved(true);
				} catch (DependencyResolveException e) {
					e.printStackTrace();
				} catch (DependencyFetchException e) {
					e.printStackTrace();
				}
			}
		return new LinkedList<Command>();
	}

}