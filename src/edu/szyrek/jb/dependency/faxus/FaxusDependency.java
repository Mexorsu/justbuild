package edu.szyrek.jb.dependency.faxus;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.PreCompiledDependency;
import edu.szyrek.faxus.cpp.CPPDynamicLib;
import edu.szyrek.faxus.cpp.CPPStaticLib;
import edu.szyrek.faxus.cpp.Compiler;
import edu.szyrek.jb.BuildJob;
import edu.szyrek.jb.BuilderNotFoundException;
import edu.szyrek.jb.Result;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetchException;
import edu.szyrek.jb.exception.BuildFailedException;
import edu.szyrek.jb.exception.BuildSetupException;
import edu.szyrek.jb.exception.DependencyResolveException;
import edu.szyrek.jb.input.IncludeInput;
import edu.szyrek.jb.input.LibraryInput;
import edu.szyrek.jb.input.LibraryPathInput;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public class FaxusDependency extends Dependency {
	private PreCompiledDependency innerDep;
	private FaxusDepType deptype;
	private final static Logger logger = Logger.getLogger("FaxusDependency");
	
	public static enum FaxusDepType {
		CPP_STATIC_LIB, CPP_DYNAMIC_LIB
	}
	
	private FaxusDependency(String name, String version) {
		super(name, version);
	}
	
	public static FaxusDependency newStaticLib(String name, String version, String compilerName, boolean debug) {
		FaxusDependency dep = new FaxusDependency(name, version);
		dep.deptype = FaxusDepType.CPP_STATIC_LIB;
				dep.innerDep = new CPPStaticLib(
						name,
						new Architecture(System.getProperty("os.arch")),
						edu.szyrek.faxus.System.fromString(System.getProperty("os.name")),
						Compiler.fromString(compilerName),
						version,
						debug);
		return dep;
	}
	public static FaxusDependency newDynamicLib(String name, String version, String compilerName, String stdLibVer, boolean debug) {
		FaxusDependency dep = new FaxusDependency(name, version);
		dep.deptype = FaxusDepType.CPP_DYNAMIC_LIB;
				dep.innerDep = new CPPDynamicLib(
						name,
						new Architecture(System.getProperty("os.arch")),
						edu.szyrek.faxus.System.fromString(System.getProperty("os.name")),
						Compiler.fromString(compilerName),
						version,
						stdLibVer,
						debug);
		return dep;
	}

	@Override
	public Collection<Input<?>> resolve(Directory dir)
			throws DependencyResolveException {
		System.out.println("Resolving "+ this.deptype +" "+this.toString()+ " in dir "+dir.getPath());
		Set<Input<?>> outputs = new HashSet<Input<?>>();
		if (this.deptype == FaxusDepType.CPP_STATIC_LIB){
			try {
				String libPath = dir.getPath();
				outputs.add(new LibraryPathInput(libPath));
				String libName = this.getInnerDep().getName()+"."+this.getInnerDep().getVersion();
				outputs.add(new LibraryInput(libName));
				String includesPath = dir.getPath()+File.separator+"includes";
				outputs.add(new IncludeInput(includesPath));
			} catch (IOException e) {
				throw new DependencyResolveException(e);
			}
		} else if (this.deptype == FaxusDepType.CPP_DYNAMIC_LIB) {
			try {
				String libFileName = "lib" + innerDep.getName() + ".so." + innerDep.getVersion();
				String libPath = dir.getPath();
								
				String libFilePath = dir.getPath()+File.separator+"lib"+innerDep.getName()+".so."+innerDep.getMajorVer()+"."+innerDep.getMinotVer()+ (innerDep.getRelease()==null ? "" : "."+innerDep.getRelease());
				String sonamePath = dir.getPath()+File.separator+"lib"+innerDep.getName()+".so."+innerDep.getMajorVer();
				String compilerNamePath = dir.getPath()+File.separator+"lib"+innerDep.getName()+".so"; 
				Files.deleteIfExists(Paths.get(sonamePath));
				Files.deleteIfExists(Paths.get(compilerNamePath));
;				
				Files.createSymbolicLink(Paths.get(sonamePath), Paths.get(libFilePath));
				
				Files.createSymbolicLink(Paths.get(compilerNamePath), Paths.get(sonamePath));
								
				outputs.add(new LibraryPathInput(libPath));
				String libName = innerDep.getName();
				outputs.add(new LibraryInput(libName));
				String includesPath = dir.getPath()+File.separator+"includes";
				outputs.add(new IncludeInput(includesPath));
			} catch (IOException e) {
				throw new DependencyResolveException(e);
			}
		}
		return outputs;
	}

	public PreCompiledDependency getInnerDep() {
		return innerDep;
	}

	
}
