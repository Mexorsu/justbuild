package edu.szyrek.jb.dependency.faxus;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.szyrek.faxus.PreCompiledDependency;
import edu.szyrek.faxus.client.FaxusClient;
import edu.szyrek.faxus.filesystem.DependencyRepo;
import edu.szyrek.faxus.filesystem.NoSuchDependencyException;
import edu.szyrek.jb.Result;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetcher;
import edu.szyrek.jb.util.BBDownloader;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Downloader;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.Unzipper;

public class FaxusDependencyFetcher extends DependencyFetcher {
	DependencyRepo repo;
	String faxusUrl;
	int faxusPort;
	Logger logger = Logger.getLogger("FaxusDependencyFetcher");
	
	public FaxusDependencyFetcher(String faxusUrl, int faxusPort) {
			this.faxusPort= faxusPort;
			this.faxusUrl = faxusUrl;
	}
	
	@Override
	public Directory fetch(Dependency dependency) throws URISyntaxException, NoSuchDependencyException {
		if (!(dependency instanceof FaxusDependency)){
			throw new NoSuchDependencyException(dependency.toString());
		}
		try {
			FaxusClient client = new FaxusClient(faxusUrl, faxusPort);
			repo = new DependencyRepo();
			FaxusDependency faxusDep = (FaxusDependency) dependency;
			PreCompiledDependency dep = faxusDep.getInnerDep();
			client.requestDependency(dep);
			File dependencyArchive = File.open(repo.getPath(dep).getAbsolutePath());
			Unzipper unzipper = new Unzipper(dependencyArchive.getPath());
			String targetPath = dependencyArchive.getPath().substring(0, dependencyArchive.getPath().lastIndexOf(".tar.gz"));
			
			unzipper.unzip(targetPath);
			Files.delete(Paths.get(dependencyArchive.getPath()));
			Directory result = Directory.open(targetPath);
			dependency.setFeched(true);
			return result;
		} catch (IOException e) {
			throw new NoSuchDependencyException("Unable to connect to faxus @"+faxusUrl+":"+faxusPort);
			
		}
	}

}
