package edu.szyrek.jb.dependency;

import edu.szyrek.jb.exception.CausedException;

public class DependencyFetchException extends CausedException{
	public DependencyFetchException(String message) {
		super(message);
	}
	public DependencyFetchException(Exception t) {
		super(t);
	}
}
