package edu.szyrek.jb.dependency.module;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

import edu.szyrek.faxus.filesystem.DependencyRepo;
import edu.szyrek.faxus.filesystem.NoSuchDependencyException;
import edu.szyrek.jb.BuildJob;
import edu.szyrek.jb.BuilderDesc;
import edu.szyrek.jb.BuilderNotFoundException;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.api.Settings;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetcher;
import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.exception.ModulesDiscoveryException;
import edu.szyrek.jb.input.AppNameInput;
import edu.szyrek.jb.input.VersionInput;
import edu.szyrek.jb.util.BBDownloader;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Downloader;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.ScriptWrapper;
import edu.szyrek.jb.util.Unzipper;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public class ModuleFetcher extends DependencyFetcher {
	DependencyRepo repo;
	Directory modulesDir;
	Logger logger = Logger.getLogger("BBDependencyFetcher");
	
	class Entry {
		public String name;
		public String version;
		
		public Entry(String name, String  version) {
			this.name = name;
			this.version = version;
		}
		
		@Override
		public boolean equals(Object o) {
			if (o == null ||!(o instanceof Entry)) {
				return false;
			}
			Entry other = (Entry) o;
			return name.equals(other.name) && version.equals(other.version);
		}
		
		@Override
		public int hashCode() {
			return Objects.hash(name, version);
		}
	}
	
	class Data {
		public Directory dir;
		public FileTime lastModified;
		public Data(Directory d, FileTime l){
			this.dir = d;
			this.lastModified = l;
		}
	}
	
	Map<Entry, Data> dirForDep = new HashMap<>();
	
	public ModuleFetcher(Directory modulesDir) throws ModulesDiscoveryException {

		try {
			List<File> buildScripts = modulesDir.findFiles(".*\\.jb",10);
			repo = new DependencyRepo();
			for (File script: buildScripts) {
				GroovyScriptEngine scriptEngine = new GroovyScriptEngine(script.getDirectory().getPath());
				BuilderDesc builder = BuildJob.loader.getBuilder(script.getName());
				File wrappedSettings = script.getTmpCopy();
		        try (FileWriter writer = new FileWriter(wrappedSettings.getUnderlyingFile(), false)){
		            writer.write(ScriptWrapper.wrap(script, builder.getSettingsScript().getName()));
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        if (builder.getOtherFiles()!=null){
			        for (File additionalFile: builder.getOtherFiles()) {
			            scriptEngine.loadScriptByName(additionalFile.getPath());
			        }
		        }
		        scriptEngine.loadScriptByName(builder.getSettingsScript().getPath());
		        @SuppressWarnings("unchecked")
		        Class<Settings> settingsClass = (Class<Settings>) scriptEngine.loadScriptByName(wrappedSettings.getPath());
				Constructor<Settings> builderConstructor = settingsClass.getDeclaredConstructor(Directory.class);
				Object settingsObject = builderConstructor.newInstance(script.getDirectory());
				Settings settings = (Settings) settingsObject;
				try {
					Files.delete(Paths.get(wrappedSettings.getPath()));
				}catch (NoSuchFileException e){
					logger.warning("Could not remove "+wrappedSettings.getPath());
				}
				settings.initialize();
				boolean moduleNameSet = false;
				boolean versionSet = false;
				
				String moduleName = "";
				String version = "";
				
				for (Input<?> in: settings.getInputs()) {
					if (in instanceof AppNameInput) {
						moduleName = (String)in.getValue();
						moduleNameSet = true;
					}
					if (in instanceof VersionInput) {
						version = (String)in.getValue();
						versionSet = true;
					}
					if (moduleNameSet&&versionSet) break;
				}
				BasicFileAttributes attr = Files.readAttributes (Paths.get(script.getDirectory().getPath()), BasicFileAttributes.class);
				dirForDep.put(new Entry(moduleName,version), new Data(script.getDirectory(),attr.lastModifiedTime()));
			}
		} catch (IOException | BuilderNotFoundException | InstantiationException | IllegalAccessException | ResourceException | ScriptException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new ModulesDiscoveryException(e1);
		}

	}
	
	@Override
	public Directory fetch(Dependency dependency) throws IOException, URISyntaxException, NoSuchDependencyException {
		if (!(dependency instanceof Module)){
			throw new NoSuchDependencyException(dependency.toString());
		}
		String dependencyArchivePath = repo.getBaseDir().toString()+File.separator+"modules"+File.separator+dependency.getName()+File.separator+dependency.getVersion();
		
		
		Module module = (Module) dependency;
		Entry key = new Entry(module.getName(), module.getVersion());
		Directory result = null;
		if (!dirForDep.containsKey(key)){
			throw new NoSuchDependencyException("Could not locate module:"+module.getName()+" "+module.getVersion());
		}
		if (Files.exists(Paths.get(dependencyArchivePath))){
			Directory dir = Directory.open(dependencyArchivePath);
			BasicFileAttributes cachedAttr = Files.readAttributes (Paths.get(dir.getPath()), BasicFileAttributes.class);
			if (cachedAttr.lastModifiedTime().compareTo(dirForDep.get(key).lastModified)>0) {
				dependency.setFeched(true);
				result = dir;
			}else {
				result = dirForDep.get(key).dir;
			}
			dependency.setFeched(true);
		}else{
			result = dirForDep.get(key).dir;
			dependency.setFeched(true);
		}
		
		return result;
	}

}
