package edu.szyrek.jb.dependency.bb;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.szyrek.faxus.filesystem.DependencyRepo;
import edu.szyrek.faxus.filesystem.NoSuchDependencyException;
import edu.szyrek.jb.Result;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetcher;
import edu.szyrek.jb.util.BBDownloader;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Downloader;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.Unzipper;

public class BBDependencyFetcher extends DependencyFetcher {

	DependencyRepo repo;
	String user;
	String password;
	Logger logger = Logger.getLogger("BBDependencyFetcher");
	
	public BBDependencyFetcher(String user, String pass) {
		this.user = user;
		this.password = pass;
	}
	
	@Override
	public Directory fetch(Dependency dependency) throws IOException, URISyntaxException, NoSuchDependencyException {
		if (!(dependency instanceof Module)){
			throw new NoSuchDependencyException(dependency.toString());
		}
		repo = new DependencyRepo();
		Module bbdep = (Module) dependency;
		Directory target = Directory.openOrCreate(repo.getBaseDir()+File.separator+bbdep.getProvider()+File.separator+bbdep.getName()+File.separator+bbdep.getVersion());
		Downloader downloader = new BBDownloader(bbdep.getProvider(), bbdep.getName(), Integer.parseInt(bbdep.getVersion().charAt(0)+""), user, password);
		String tarPath = target.getPath()+File.separator+"temp.tar.gz";
		downloader.download(tarPath);
		Unzipper unzipper = new Unzipper(tarPath);
		
		unzipper.unzip(target.getPath());
		dependency.setFeched(true);
		return target;
	}

}
