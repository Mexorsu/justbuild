package edu.szyrek.jb.dependency.bb;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import edu.szyrek.jb.BuildJob;
import edu.szyrek.jb.BuildResult;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.DependencyFetchException;
import edu.szyrek.jb.exception.BuildFailedException;
import edu.szyrek.jb.exception.BuildSetupException;
import edu.szyrek.jb.exception.DependencyResolveException;
import edu.szyrek.jb.input.ExportHeaderInput;
import edu.szyrek.jb.input.IncludeInput;
import edu.szyrek.jb.input.LibraryInput;
import edu.szyrek.jb.input.LibraryPathInput;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;

public class Module extends Dependency {
	private String provider;
	public static final String VERSION_SEPARATOR = "/";
	
	public Module(String name, String version) {
		super(name, version);
	}

	@Override
	public Collection<Input<?>> resolve(Directory dir) throws DependencyResolveException {
		try {
			File settings = (File)dir.find(".*.jb", 0).get(0);
			if (!settings.exists()) {
				throw new DependencyFetchException("Could not resolve JBDependency " + this.toString() + ", not a valid build file: " + settings.toString());
			}
			System.setProperty("user.dir", settings.getDirectory().getPath());
			BuildJob job;
			Set<Input<?>> outputs = new HashSet<Input<?>>();
			job = new BuildJob(settings);
			BuildResult buildResult = job.build();
			System.out.println("Build "+this.toString()+": "+buildResult);
			Directory includeDir;
			includeDir = Directory.openOrCreate(dir.getPath()+"/.jbexport");
			outputs.add(new IncludeInput(includeDir.getPath()));
			
			for (Input artifact: buildResult.getArtifacts()) {
				try {
					if (artifact instanceof LibraryInput) {
						outputs.add(artifact);
					} else if (artifact instanceof LibraryPathInput) {
						outputs.add(artifact);
					} else if (artifact instanceof ExportHeaderInput) {
						File headerFile = (File) artifact.getValue();
						Files.copy(Paths.get(headerFile.getPath()), Paths.get(includeDir.getPath()+File.separator+headerFile.getLastPathCopmonent()), StandardCopyOption.REPLACE_EXISTING);
					}
				} catch (IOException e) {
					System.err.println("Failed to resolve artifact: " + artifact.getDescription());
					e.printStackTrace();
				}
			}
		
			return outputs;
		} catch (BuildSetupException|BuildFailedException|DependencyFetchException | NotDirectoryException | FileNotFoundException e) {
			throw new DependencyResolveException(e);
		}
		
		
	}
	
	
	@Override
	public String toString() {
		return this.provider + File.separator + super.toString();
	}
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	
	
	
//	return Directory.open(repositoryDir.getPath() + File.separator +
//			dep.getProvider() + File.separator + 
//			dep.getName() + File.separator +
//			dep.getVersion());
	
	
}
