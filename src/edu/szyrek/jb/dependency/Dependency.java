package edu.szyrek.jb.dependency;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Collection;

import edu.szyrek.faxus.filesystem.NoSuchDependencyException;
import edu.szyrek.jb.BuilderNotFoundException;
import edu.szyrek.jb.Result;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.exception.BuildFailedException;
import edu.szyrek.jb.exception.DependencyResolveException;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public abstract class Dependency {
	private String name;
	protected String version;
	private boolean resolved = false;
	private boolean feched = false;
	
	public boolean isResolved() {
		return resolved;
	}

	public boolean isFeched() {
		return feched;
	}

	public void setFeched(boolean feched) {
		this.feched = feched;
	}

	public void setResolved(boolean resolved) {
		this.resolved = resolved;
	}
	public Dependency() {
	}
	public Dependency(String name, String version) {
		super();
		this.name = name;
		this.version = version;
	}
	
	public Directory fetch(DependencyFetcher fetcher) throws DependencyFetchException {
		Directory res;
		try {
			res = fetcher.fetch(this);
			return res;
		} catch (IOException | URISyntaxException | NoSuchDependencyException e) {
				throw new DependencyFetchException(e);
		}
		
	}
	
	public abstract Collection<Input<?>> resolve(Directory dir) throws DependencyResolveException;
	
	
	@Override
	public String toString() {
		return  File.separator + this.name + File.separator + this.version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
