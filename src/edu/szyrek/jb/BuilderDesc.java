package edu.szyrek.jb;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import edu.szyrek.jb.api.Builder;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;

public class BuilderDesc {
	private String name;
	private File descFile;
	private Directory builderDir;
	private File builderScript;
	private File settingsScript;
	private List<File> otherFiles;
	private Class<? extends Builder> builderClass;
	private Class<? extends BaseSettings> settingsClass;
	
	public BuilderDesc(File descFile) {
		super();
		Properties builderConfig = new Properties();
		try (FileInputStream configFile = new FileInputStream(descFile.getPath())){
			builderConfig.load(configFile);
		}catch (Exception e){
			System.err.println("Failed parsing builders configuration");
			e.printStackTrace();
		}
		this.name = name;
		this.builderDir = builderDir;
		this.builderScript = builderScript;
		this.settingsScript = settingsScript;
		this.otherFiles = otherFiles;
	}
	
	public BuilderDesc(String name, Directory builderDir, File builderScript,
			File settingsScript, List<File> otherFiles) {
		super();
		this.name = name;
		this.builderDir = builderDir;
		this.builderScript = builderScript;
		this.settingsScript = settingsScript;
		this.otherFiles = otherFiles;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public File getDescFile() {
		return descFile;
	}

	public void setDescFile(File descFile) {
		this.descFile = descFile;
	}

	public Directory getBuilderDir() {
		return builderDir;
	}

	public void setBuilderDir(Directory builderDir) {
		this.builderDir = builderDir;
	}

	public File getBuilderScript() {
		return builderScript;
	}

	public void setBuilderScript(File builderScript) {
		this.builderScript = builderScript;
	}

	public File getSettingsScript() {
		return settingsScript;
	}

	public void setSettingsScript(File settingsScript) {
		this.settingsScript = settingsScript;
	}

	public List<File> getOtherFiles() {
		return otherFiles;
	}

	public void setOtherFiles(List<File> otherFiles) {
		this.otherFiles = otherFiles;
	}

	public Class<? extends Builder> getBuilderClass() {
		return builderClass;
	}

	public void setBuilderClass(Class<? extends Builder> builderClass) {
		this.builderClass = builderClass;
	}

	public Class<? extends BaseSettings> getSettingsClass() {
		return settingsClass;
	}

	public void setSettingsClass(Class<? extends BaseSettings> settingsClass) {
		this.settingsClass = settingsClass;
	}

	
	
	
}
