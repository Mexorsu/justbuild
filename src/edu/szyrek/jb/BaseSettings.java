package edu.szyrek.jb;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import edu.szyrek.jb.api.Discovery;
import edu.szyrek.jb.api.Input;
import edu.szyrek.jb.api.Settings;
import edu.szyrek.jb.api.Target;
import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.Path;
import edu.szyrek.jb.input.AppNameInput;
import edu.szyrek.jb.input.FaxusDynamicLibInput;
import edu.szyrek.jb.input.FaxusStaticLibInput;
import edu.szyrek.jb.input.ModuleInput;
import edu.szyrek.jb.input.ModulesDirInput;
import edu.szyrek.jb.input.OutDir;
import edu.szyrek.jb.input.SourceInput;
import edu.szyrek.jb.input.TargetInput;
import edu.szyrek.jb.input.VersionInput;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public abstract class BaseSettings implements Settings {
	public Directory buildRoot;
	public String builder;
	public Set<Input<?>> inputs = new HashSet<>();
	private Logger logger = Logger.getLogger(this.getClass().getCanonicalName());  
	private boolean modular = false;
	
	public BaseSettings(Directory root) {
		this.buildRoot = root;
	}

	@Override
	public abstract void initialize() throws ResourceException, ScriptException, IOException, InstantiationException, IllegalAccessException;
	
	public void addInputs(Collection<Input<?>> inputs) {
		for (Input<?> in: inputs) {
			addInput(in);
		}
	}
	
	public void addInput(Input<?> input) {
		if (inputs.contains(input)){
			inputs.remove(input);
			inputs.add(input);
			logger.warning("Overwriting "+input);
		}else{
			inputs.add(input);
		}
	}

	@Override
	public Set<Input<?>> getInputs() {		
		return this.inputs;
	}

	@Override
	public String getBuilder() {
		return builder;
	}

	public void appName(String name) {
		addInput(new AppNameInput(name));
	}
	
	public void version(String ver) {
		addInput(new VersionInput(ver));
	}
	
	public void outDir(String path) throws NotDirectoryException, FileAlreadyExistsException {
		addInput(new OutDir(path));
	}
	
	public void target(Target t) {
		addInput(new TargetInput(t));
	}
	
	public void addSource(String path) throws FileNotFoundException {
		this.addInput(new SourceInput(path));
	}

	public void modulesRoot(String path) throws FileAlreadyExistsException, NotDirectoryException {
		if(!modular){
			modular = true;
			this.addInput(new ModulesDirInput(path));
		}
	}
	
	public void module(String name, String version) throws FileAlreadyExistsException, NotDirectoryException {
		if(!modular){
			modular = true;
			this.addInput(new ModulesDirInput(this.getBuildRoot().getPath()));
		}
		this.addInput(new ModuleInput(name, version));
	}
	
	public void addSourceDir(String path) throws NotDirectoryException, FileNotFoundException {
		Directory sourceDir = Directory.open(path);
		for (Path f: sourceDir.getChildren()) {
			if (f instanceof File) {
				this.addInput(new SourceInput(f.getPath()));
			}
		}
	}
	public void discover(Discovery disc) throws NotDirectoryException, FileNotFoundException {
		for (Path p: getBuildRoot().find(
									disc.includes(),
									disc.excludes(),
									Discovery.DEFAULT_DEPTH)) {
			Input<?> in = disc.pathToInput(p);
			this.addInput(in);
		}		
	}
	
	@Override
	public Directory getBuildRoot() {
		return buildRoot;
	}
}
