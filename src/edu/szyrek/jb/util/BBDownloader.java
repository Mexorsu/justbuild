package edu.szyrek.jb.util;

public class BBDownloader extends HTTPSDownloader {

	public BBDownloader(String url, String authUser, String password) {
		super(url, authUser, password);
	}
	public BBDownloader(String bbuser, String bbrepo, int version, String authUser, String password) {
		super("https://bitbucket.org/"+bbuser+"/"+bbrepo+"/get/ver"+version+".tar.gz", authUser, password);
	}
}
