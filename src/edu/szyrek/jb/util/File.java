package edu.szyrek.jb.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class File implements edu.szyrek.jb.util.Path {
    public static String separator = java.io.File.separator;
    public static final String BACKUP_EXT = ".bak";
    public static final String TMP_EXT = ".tmp";
    private java.io.File wrapped;

    public String getName() {
        return wrapped.getName().replace("."+getExtension(),"");
    }
    public String getLastPathCopmonent() {
        return wrapped.getName();
    }

    @Override
    public String toString() {
        return this.getPath();
    }

    private File(java.io.File underlying) {
        this.wrapped = underlying.getAbsoluteFile();
    }
    public static File open(String path) throws FileNotFoundException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (!f.exists()){
            throw new FileNotFoundException("File "+path+" does not exist");
        }else if (f.isDirectory()) {
            throw new IllegalStateException(path+" is directory");
        }
        return new File(f); 
    }
    public static File create(String path) throws FileAlreadyExistsException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (f.exists()){
            throw new FileAlreadyExistsException("File "+path+" already exist");
        }
        return new File(f); 
    }
    public static File createOrOpen(String path) throws IOException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (!f.exists()){
            f.createNewFile();
        }
        return new File(f); 
    }
    public String getExtension() {
        int i = wrapped.getPath().lastIndexOf('.');
        if (i>0) {
            return wrapped.getPath().substring(i+1);
        }
        return "";
    }
    
    public File getTmpCopy() throws IOException {
        File tmpFile = File.createOrOpen(this.getDirectory().getPath()+File.separator+this.getName()+this.getExtension()+TMP_EXT);
        Path source = Paths.get(this.getPath());
        Path dest = Paths.get(tmpFile.getPath());
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        return tmpFile;
    }

    public void backup() throws IOException {
        String originalFilePath = wrapped.getAbsolutePath();
        String extension = getExtension();
        String backupFilePath = originalFilePath.replace(extension,extension+BACKUP_EXT);
        Path source = Paths.get(originalFilePath);
        Path dest = Paths.get(backupFilePath);
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
    }

    public void reloadFromBackup() throws IOException {
        String originalFilePath = wrapped.getAbsolutePath();
        String extension = getExtension();
        String backupFilePath = originalFilePath.replace(extension,extension+BACKUP_EXT);
        Path dest = Paths.get(originalFilePath);
        Path source = Paths.get(backupFilePath);
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
    }

    public Directory getDirectory() {
        try {
            return Directory.open(wrapped.getParent());
        } catch(FileNotFoundException|NotDirectoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPath() {
        return wrapped.getAbsolutePath();
    }

    public java.io.File getUnderlyingFile() {
        return wrapped;
    }

    public boolean exists() {
        return wrapped.exists();
    }
    

	@Override
	public String getDescription() {
		return "File: ["+this.getPath()+"]";
	}
}
