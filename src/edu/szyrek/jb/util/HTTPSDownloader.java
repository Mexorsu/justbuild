package edu.szyrek.jb.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HTTPSDownloader implements Downloader {
	private static TrustManager[] trustAllCerts = new TrustManager[]{
		    new X509TrustManager() {
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		        }
		        public void checkClientTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		        public void checkServerTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) {
		        }
		    }
		};
	static {
		// Activate the new trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("SSL");
		    sc.init(null, trustAllCerts, new java.security.SecureRandom());
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}
	}
	String urlString;
	String authUser;
	String password;

	static class MyAuthenticator extends Authenticator {
		String user;
		String password;
		public MyAuthenticator(String user, String password) {
			this.user = user;
			this.password = password;
		}
        public PasswordAuthentication getPasswordAuthentication() {
            // I haven't checked getRequestingScheme() here, since for NTLM
            // and Negotiate, the usrname and password are all the same.
            System.err.println("Feeding username and password for " + getRequestingScheme());
            return (new PasswordAuthentication(user, password.toCharArray()));
        }
    }
	
	public void download(String target) throws IOException {
		// And as before now you can use URL and URLConnection

		String authString = authUser + ":" + password;
		System.out.println("auth string: " + authString);
		byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		System.out.println("Base64 encoded auth string: " + authStringEnc);
		
		URL url = new URL(urlString);
		URLConnection connection = url.openConnection();
		connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
		try (InputStream in = connection.getInputStream();
			FileOutputStream out = new FileOutputStream(target)){

			int read = 0;
			byte[] bytes = new byte[1024];
			
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public HTTPSDownloader(String url, String authUser, String password) {
		this.urlString = url;
		this.authUser = authUser;
		this.password = password;
	}

}

