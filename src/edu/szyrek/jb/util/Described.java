package edu.szyrek.jb.util;

public interface Described {
	public String getDescription();
}
