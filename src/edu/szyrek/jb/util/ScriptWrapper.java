package edu.szyrek.jb.util;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScriptWrapper {
    public static String wrap(File buildScript, String baseSettingsClassName) {
        StringBuilder sb = new StringBuilder();
        sb.append("import edu.szyrek.jb.util.*\n");
        sb.append("import edu.szyrek.jb.*\n");
        sb.append("import edu.szyrek.jb.input.*\n");
        sb.append("import edu.szyrek.jb.api.*\n");
        sb.append("\n");
        sb.append("class BuildScript extends "+baseSettingsClassName+" {\n");
        sb.append("    public BuildScript (Directory root) {\n");
        sb.append("        super(root)\n");
        sb.append("    }\n");
        sb.append("    @Override\n");
        sb.append("    public void initialize() {\n        ");
        try (Scanner scanner = new Scanner(buildScript.getUnderlyingFile())){
            String buildScriptContent = scanner.useDelimiter("\\Z").next().replace("\n","\n        ");
            sb.append(buildScriptContent + "\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        sb.append("    }\n");
        sb.append("}\n");
        return sb.toString();
    }
}


