package edu.szyrek.jb.util;

import java.util.LinkedList;

public class DoubleLinkedList<T> extends LinkedList<T> {
    static final long serialVersionUID = 665L;
	public void addBefore(T newElement, T anchorElement){
		int index = this.indexOf(anchorElement);
		this.add(index, newElement);
	}
	public void addAfter(T newElement, T anchorElement){
		int index = this.indexOf(anchorElement);
		this.add(index+1, newElement);
	}
}
