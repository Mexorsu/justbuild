package edu.szyrek.jb.util;

import java.io.IOException;

public interface Downloader {
	public void download(String target) throws IOException;
}
