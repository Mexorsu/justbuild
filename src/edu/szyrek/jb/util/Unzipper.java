package edu.szyrek.jb.util;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

public class Unzipper {
	final static int BUFFER = 2048;

	String path;
	public Unzipper(String path) {
		this.path = path;
	}
	
	public void unzip(String target) throws URISyntaxException, IOException {
		TarArchiveEntry entry = null;
		Path dir = Paths.get(target);
		Files.createDirectories(dir);
		try (TarArchiveInputStream in = new TarArchiveInputStream(new GZIPInputStream(new FileInputStream(path)));){
			
				while ((entry = (TarArchiveEntry) in.getNextEntry()) != null) {
					   String fileName = entry.getName();
					   if (entry.getName().contains(File.separator)) {
						   fileName = entry.getName().substring(entry.getName().indexOf(File.separator), entry.getName().length());
					   }
					   System.out.println("Extracting: " + target + File.separator + fileName);
					   if (entry.isDirectory()) {
					    File f = new File(target + File.separator + fileName);
					    f.mkdirs();
					   } else {
					    int count;
					    byte data[] = new byte[BUFFER];
					 
					    FileOutputStream fos = new FileOutputStream(target
					      + File.separator +  fileName);
					    BufferedOutputStream dest = new BufferedOutputStream(fos,
					      BUFFER);
					    while ((count = in.read(data, 0, BUFFER)) != -1) {
					     dest.write(data, 0, count);
					    }
					    dest.close();
					   }
					  }
			}catch (Exception e) {
				e.printStackTrace();
			}
	}
}
