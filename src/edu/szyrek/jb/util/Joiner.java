package edu.szyrek.jb.util;

import java.util.Collection;

public class Joiner {
	public static String join(Collection<?> col) {
		if (col==null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Object o: col){
			if (!first){
				sb.append(", "+o.toString());				
			}else{
				sb.append(o.toString());
				first = false;
			}
		}
		return sb.toString();
	}
}
