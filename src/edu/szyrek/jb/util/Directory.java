package edu.szyrek.jb.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Directory implements Path {
    private File file;

    private Directory(File dir) {
        this.file = dir;
    }

    public String getName() {
        return file.getName();
    }

    
    public static Directory openOrCreate(String path) throws NotDirectoryException {
        if (path == null || path == "") {
            throw new NotDirectoryException("Empty path!");
        }
        File dir = new File(path);
        if (!dir.exists()) {
        	dir.mkdirs();
        }
        return new Directory(dir);
    }
    
    public static Directory create(String path) throws FileAlreadyExistsException, NotDirectoryException {
        if (path == null || path == "") {
            throw new NotDirectoryException("Empty path!");
        }
        File dir = new File(path);
        if (dir.exists()) {
            throw new FileAlreadyExistsException("File "+path+" already exists!");
        }
        dir.mkdirs();
        return new Directory(dir);
    }
    public static Directory open(String dirPath) throws FileNotFoundException, NotDirectoryException {
        if (dirPath == null || dirPath == "") {
            throw new NotDirectoryException("Empty path!");
        }
        File dir = new File(dirPath);
        if(!dir.isAbsolute()) {
            dir = dir.getAbsoluteFile();
        }
        if (!dir.exists()) {
            throw new FileNotFoundException("Directory '"+dirPath+"' could not be found!");
        }
        if (!dir.isDirectory()) {
            throw new NotDirectoryException("'"+dirPath+"' is not a directory!");
        }
        return new Directory(dir);
    }
    
    @Override
    public String toString() {
    	return this.getPath();
    }
    
    public String getPath() {
        return this.file.getAbsolutePath();
    }
    public File getFile() {
        return this.file;
    }
    
    public List<Path> find(List<String> includes, List<String> excludes, int maxDepth) {
    	if (includes==null){
    		includes = new ArrayList<>();
    	}
    	if (excludes==null){
    		excludes= new ArrayList<>();
    	}
    	if (maxDepth < 0){
    		throw new IllegalStateException("MaxDepth must be greater than or equal to 0");
    	}
    	
    	List<Path> result = new ArrayList<>();
    	Queue<Directory> toSearch = new LinkedList<>();
    	Map<Path, Integer> dirDepth = new HashMap<>();
    	toSearch.add(this);
    	dirDepth.put(this, 0);
    	
    	while(!toSearch.isEmpty()){
    		Directory searched = toSearch.poll();
    		if (dirDepth.get(searched) > maxDepth) break;
    		pathsloop:
    		for (Path p: searched.getChildren()) {
    			if (p == null) continue;
    			if (p instanceof Directory){
    				Directory dir = (Directory) p;
    				toSearch.offer(dir);
    				dirDepth.put(p, (dirDepth.get(searched)+1));
    				boolean matched = false;
    				for (String regex: includes){
    					if (Pattern.matches(regex, dir.getName())){
    						matched = true;
    						break;
    					}
    				}
    				for (String regex: excludes){
    					if (Pattern.matches(regex, dir.getName())){
    						continue pathsloop;
    					}
    				}
    				if (matched||includes.size()==0){
    					result.add(p);
    				}
    			}else if (p instanceof edu.szyrek.jb.util.File){
    				edu.szyrek.jb.util.File file = (edu.szyrek.jb.util.File) p;
    				boolean matched = false;
      				for (String regex: includes){
    					if (Pattern.matches(regex, file.getLastPathCopmonent())){
    						matched = true;
    						break;
    					}
    				}
    				for (String regex: excludes){
    					if (Pattern.matches(regex, file.getLastPathCopmonent())){
    						continue pathsloop;
    					}
    				}
    				if (matched||includes.size()==0){
    					result.add(p);
    				}
    			}else{
    				throw new IllegalStateException("What the heck is "+p);
    			}
    		}
    	}
    	return result;
    }
    
    public List<Path> find(String regex, int maxDepth) {
    	List<Path> result = new ArrayList<>();
    	Queue<Directory> toSearch = new LinkedList<>();
    	Map<Path, Integer> dirDepth = new HashMap<>();
    	toSearch.add(this);
    	dirDepth.put(this, 0);
    	
    	while(!toSearch.isEmpty()){
    		Directory searched = toSearch.poll();
    		if (dirDepth.get(searched) > maxDepth) break;
    		for (Path p: searched.getChildren()) {
    			if (p == null) continue;
    			if (p instanceof Directory){
    				Directory dir = (Directory) p;
    				toSearch.offer(dir);
    				dirDepth.put(p, (dirDepth.get(searched)+1));
    				if (Pattern.matches(regex, dir.getName())){
    					result.add(p);
    				}
    			}else if (p instanceof edu.szyrek.jb.util.File){
    				edu.szyrek.jb.util.File file = (edu.szyrek.jb.util.File) p;
    				if (Pattern.matches(regex, file.getLastPathCopmonent())){
    					result.add(p);
    				}
    			}else{
    				throw new IllegalStateException("What the heck is "+p);
    			}
    		}
    	}
    	return result;
    }
    public List<edu.szyrek.jb.util.File > findFiles(String regex, int maxDepth) {
    	List<edu.szyrek.jb.util.File > result = new ArrayList<>();
    	Queue<Directory> toSearch = new LinkedList<>();
    	Map<Path, Integer> dirDepth = new HashMap<>();
    	toSearch.add(this);
    	dirDepth.put(this, 0);
    	
    	while(!toSearch.isEmpty()){
    		Directory searched = toSearch.poll();
    		if (dirDepth.get(searched) > maxDepth) break;
    		for (Path p: searched.getChildren()) {
    			if (p == null) continue;
    			if (p instanceof Directory){
    				Directory dir = (Directory) p;
    				toSearch.offer(dir);
    				dirDepth.put(p, (dirDepth.get(searched)+1));
    			}else if (p instanceof edu.szyrek.jb.util.File){
    				edu.szyrek.jb.util.File file = (edu.szyrek.jb.util.File) p;
    				if (Pattern.matches(regex, file.getLastPathCopmonent())){
    					result.add((edu.szyrek.jb.util.File)p);
    				}
    			}else{
    				throw new IllegalStateException("What the heck is "+p);
    			}
    		}
    	}
    	return result;
    }
    @SuppressWarnings("unchecked")
	public List<Path> getChildren() {
    	return (List<edu.szyrek.jb.util.Path>)(Object) 
    			Arrays.asList(file.listFiles())
    			.stream()
    			.map(f ->{
	    				try {
	    					if (f.isDirectory()) {
	    						return Directory.open(f.getAbsolutePath());
	    					}else{
	    						return edu.szyrek.jb.util.File.open(f.getAbsolutePath());
	    					}
	    				}catch (FileNotFoundException|NotDirectoryException e){
	    					//e.printStackTrace();
	    					return null;
	    				}
    				}
    			)
    			.collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
	public List<edu.szyrek.jb.util.File> findByName(String regex) throws FileNotFoundException {
    	return (List<edu.szyrek.jb.util.File>)(Object)
    			Arrays.asList(file.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches(regex);
			}
		})).stream().map(file-> {
									try {
										return edu.szyrek.jb.util.File.open(file.getAbsolutePath());
									} catch (Exception e) {
										return null;
									}
								}
				).collect(Collectors.toList());
    }

	@Override
	public String getDescription() {
		return "Directory: ["+this.getPath()+"]";
	}
}
