package edu.szyrek.jb;

import edu.szyrek.jb.api.Target;

public enum JBTarget implements Target {
	NOT_SET;

	@Override
	public int getCode() {
		return this.ordinal();
	}

}
