package edu.szyrek.jb;

import java.util.ArrayList;
import java.util.List;

import edu.szyrek.jb.api.Discovery;

public abstract class IncludeDiscovery implements Discovery {
	List<String> includes = new ArrayList<>();
	
	public IncludeDiscovery(String pattern) {
		includes.add(pattern);
	}

	@Override
	public List<String> includes() {
		return includes;
	}

	@Override
	public List<String> excludes() {
		return null;
	}

}
