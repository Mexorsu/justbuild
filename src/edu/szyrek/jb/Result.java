package edu.szyrek.jb;
import java.util.ArrayList;
import java.util.List;

public class Result {
	private List<String> errors = new ArrayList<String>();
	private StringBuilder output = new StringBuilder();
	private boolean passed = true;
	public static Result OK = new Result();
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(output);
		for (String err: errors){
			passed = false;
			sb.append("\t"+err+"\n");
		}
		return passed ? "Passed\n": "Failed:\n"+sb.toString();
	}
	public void addError(String err) {
		this.errors.add(err);
		this.passed = false;
	}
	public void appendOut(String s) {
		this.output.append(s);
	}
	public void appendResult(Result res) {
		this.errors.addAll(res.errors);
		this.passed = res.passed ? this.passed : res.passed;
		this.appendOut(res.getOutput());
	}
	public String getOutput() {
		return this.output.toString();
	}
	public void flushOut() {
		System.out.print(output.toString());
		output = new StringBuilder();
	}
}
