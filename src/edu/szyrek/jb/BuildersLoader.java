package edu.szyrek.jb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import com.sun.istack.internal.logging.Logger;

import edu.szyrek.jb.util.BBDownloader;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Downloader;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.Path;
import edu.szyrek.jb.util.Unzipper;

public class BuildersLoader {
	private static Map<String, BuilderDesc> builders = new HashMap<>();
	private Properties buildersConfig;	
	private Logger logger = Logger.getLogger(BuildersLoader.class);
	
	
	public void fetchBuilder(String builderName, int version) throws FileNotFoundException, IOException, URISyntaxException{
		Directory compilersDir = Directory.open(BuildJob.jbHome.getPath() +File.separator+"builders");
		Properties buildersConfig = new Properties();
		try (FileInputStream configFile = new FileInputStream(compilersDir.findByName("config").get(0).getPath())){
			buildersConfig.load(configFile);
		}catch (Exception e){
			System.err.println("Failed parsing builders configuration");
			e.printStackTrace();
		}
		
		String entry = buildersConfig.getProperty(builderName);
		String key = builderName;
		if (key == null) throw new IllegalArgumentException("No such builder: "+builderName+" defined in builders config");
			String builderString = (String) buildersConfig.get(key);
			String[] urlArr = builderString.split("/");
			if (urlArr.length!=4) throw new IllegalStateException("Misformatted entry in builders config: " +builderString);
			String name = ((String)key).replace("+", "\\+").replace("*", "\\*");
			String provider = urlArr[0];
			String repo = urlArr[1];
			int majorVer = Integer.parseInt(urlArr[2]);
			int minorVer = Integer.parseInt(urlArr[3]);
			name = ((String)key).replace("+", "\\+").replace("*", "\\*");

			if (compilersDir.find(name,1).size()==0){
				String targetPath = compilersDir.getPath()+File.separator+key;
				String tarPath = compilersDir.getPath()+File.separator+key+".tar.gz";
				Downloader downloader = new BBDownloader(provider, repo, majorVer, "mexorsu", "tater4964589");
				downloader.download(tarPath);
				Unzipper unzipper = new Unzipper(tarPath);
				unzipper.unzip(targetPath);
				Files.delete(Paths.get(new URI("file://"+tarPath)));
			}
	}
	
	public void updateBuildersRepository() throws FileNotFoundException, IOException, URISyntaxException{
		Directory compilersDir = Directory.open(BuildJob.jbHome.getPath() +File.separator+"builders");
		Properties buildersConfig = new Properties();
		try (FileInputStream configFile = new FileInputStream(compilersDir.findByName("config").get(0).getPath())){
			buildersConfig.load(configFile);
		}catch (Exception e){
			System.err.println("Failed parsing builders configuration");
			e.printStackTrace();
		}
		
		for (Object key: buildersConfig.keySet()) {
			String builderString = (String) buildersConfig.get(key);
			String[] urlArr = builderString.split("/");
			if (urlArr.length!=4) throw new IllegalStateException("Misformatted entry in builders config: " +builderString);
			String name = ((String)key).replace("+", "\\+").replace("*", "\\*");
			String provider = urlArr[0];
			String repo = urlArr[1];
			int majorVer = Integer.parseInt(urlArr[2]);
			int minorVer = Integer.parseInt(urlArr[3]);
			name = ((String)key).replace("+", "\\+").replace("*", "\\*");

			if (compilersDir.find(name,1).size()==0){
				String targetPath = compilersDir.getPath()+File.separator+key;
				String tarPath = compilersDir.getPath()+File.separator+key+".tar.gz";
				Downloader downloader = new BBDownloader(provider, repo, majorVer, "mexorsu", "tater4964589");
				downloader.download(tarPath);
				Unzipper unzipper = new Unzipper(tarPath);
				unzipper.unzip(targetPath);
				Files.delete(Paths.get(new URI("file://"+tarPath)));
			}
			
		}
	}
	
	public void init() throws IOException, URISyntaxException {
		Directory compilersDir = Directory.open(BuildJob.jbHome.getPath() +File.separator+"builders");
		buildersConfig = new Properties();
		try (FileInputStream configFile = new FileInputStream(compilersDir.findByName("config").get(0).getPath())){
			buildersConfig.load(configFile);
		}catch (Exception e){
			System.err.println("Failed parsing builders configuration");
			e.printStackTrace();
		}
		
		for (Path path: compilersDir.getChildren()){
			if (path instanceof File) continue;
			Directory dir = (Directory) path;
			loadBuilder(dir.getName(), dir);
		}

	}
	
	@SuppressWarnings("unchecked")
	public void loadBuilder(String name, final Directory dir) throws FileNotFoundException {
		Properties builderConfig = new Properties();
		try (FileInputStream descFile = new FileInputStream(dir.findByName("desc").get(0).getPath())){
			builderConfig.load(descFile);
		}catch (Exception e){
			System.err.println("Failed parsing builders configuration");
			e.printStackTrace();
		}
		
		List<File> addditionalFiles = null;
		if ((String)builderConfig.get("additional")!=null){
		addditionalFiles = (List<File>)(List<?>)Arrays.asList(((String)builderConfig.get("additional")).split(",")).stream().map(
				s -> {
					try {
						return File.open(dir.getPath()+File.separator+s);
					}catch (Exception e){
						return null;
					}
				}
				).filter(o -> o != null).collect(Collectors.toList());
		}
		BuilderDesc newDesc = new BuilderDesc(name,
				dir,
				File.open(dir.getPath()+File.separator+(String)builderConfig.get("builder")),
				File.open(dir.getPath()+File.separator+(String)builderConfig.get("settings")),
				addditionalFiles);
		builders.put(name, newDesc);
	}
	
	public BuilderDesc getBuilder(String name) throws BuilderNotFoundException {
		if (!builders.containsKey(name)) {
			if (buildersConfig.containsKey(name)) {
				try {
					logger.warning("Builder: "+name+" not loaded, downloading from "+buildersConfig.getProperty(name));
					fetchBuilder(name, Integer.parseInt(buildersConfig.get(name).toString().split("/")[2]));
					init();
				} catch (IOException|URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				throw new BuilderNotFoundException("No such builder: "+name+" defined in builders config");
			}
		}
		if (!builders.containsKey(name)) throw new BuilderNotFoundException("Builder not found: "+name);
		return builders.get(name);
	}
	
}
