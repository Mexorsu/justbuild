package edu.szyrek.jb;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import edu.szyrek.jb.api.Input;

public class BuildResult extends Result {
	public static final BuildResult OK = new BuildResult();
	private Set<Input<?>> artifacts = new HashSet<>();

	public Set<Input<?>> getArtifacts() {
		return artifacts;
	}

	public void setArtifacts(Set<Input<?>> artifacts) {
		this.artifacts = artifacts;
	}
	public void addArtifact(Input<?> artifact) {
		this.artifacts.add(artifact);
	}
	public void addArtifacts(Collection<Input<?>> artifacts) {
		for (Iterator<Input<?>> it = artifacts.iterator(); it.hasNext(); ){			
			this.artifacts.add(it.next());
		}
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()+"\n");
		if (artifacts==null||artifacts.size()==0) 
			return sb.toString();
		sb.append("Artifacts:\n");
		for (Input<?> artifact: artifacts) {
			sb.append("\t"+artifact.getDescription()+"\n");
		}
		return sb.toString();
	}
	
}
