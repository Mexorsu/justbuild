package edu.szyrek.jb;

import java.util.Objects;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import edu.szyrek.jb.api.Command;

public abstract class BuildPhase {
    static final long serialVersionUID = 6342L;
	
	public BuildPhase(){}
	
	public int id = random.nextInt();
	private static Random random= new Random(System.currentTimeMillis());
	
	public abstract Queue<Command> execute();
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ":["+this.id+"]";
	}
	
	@Override
	public boolean equals(Object another){
		if (another==null)
			return false;
		if (!BuildPhase.class.isAssignableFrom(another.getClass()))
			return false;
		BuildPhase phase = (BuildPhase) another;
		if (this.id == phase.id){
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
