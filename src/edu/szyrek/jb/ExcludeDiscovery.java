package edu.szyrek.jb;

import java.util.ArrayList;
import java.util.List;
import edu.szyrek.jb.api.Discovery;

public abstract class ExcludeDiscovery implements Discovery {
	List<String> excludes = new ArrayList<>();
	
	public ExcludeDiscovery(String pattern) {
		excludes.add(pattern);
	}

	@Override
	public List<String> includes() {
		return null;
	}

	@Override
	public List<String> excludes() {
		return excludes;
	}

}
