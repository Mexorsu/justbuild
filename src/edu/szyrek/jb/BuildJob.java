package edu.szyrek.jb;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Queue;

import com.sun.istack.internal.logging.Logger;

import edu.szyrek.jb.api.Builder;
import edu.szyrek.jb.api.Command;
import edu.szyrek.jb.api.Settings;
import edu.szyrek.jb.dependency.Dependency;
import edu.szyrek.jb.dependency.bb.Module;
import edu.szyrek.jb.dependency.bb.BBDependencyFetcher;
import edu.szyrek.jb.exception.BuildFailedException;
import edu.szyrek.jb.exception.BuildSetupException;
import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.File;
import edu.szyrek.jb.util.ScriptWrapper;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;


public class BuildJob {
	private GroovyScriptEngine scriptEngine;
	private Settings settings;
	BuilderDesc builderDescriptor;
	Builder builder;
	public Directory buildRoot;
	public static Directory jbHome;
	static public BuildersLoader loader = new BuildersLoader();
	Logger logger = Logger.getLogger(BuildJob.class);
	
	
	
	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	public static void main(String[] args) throws IOException, InterruptedException, ResourceException, ScriptException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, URISyntaxException, BuilderNotFoundException {
		jbHome = Directory.open(System.getProperty("user.home")+File.separator+".jb"); 
		loader.init();
		File settingsFile = null;
		if (args.length > 0) {
			String settingsFilePath = args[0];
			settingsFile = File.open(settingsFilePath);
			if (!settingsFile.exists()) {
				throw new IllegalArgumentException("You must specify a valid build settings path as only argument");
			}
		}else{
			throw new IllegalArgumentException("You must specify a valid build settings path as only argument");
		}
		System.setProperty("user.dir", settingsFile.getDirectory().getPath());
		try {
			BuildJob job = new BuildJob(settingsFile);
			Result buildResult = job.build();
			System.out.println(buildResult);
		} catch (BuildFailedException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (BuildSetupException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
  	public BuildJob(File settingsFile, Builder dbuilder) throws BuildSetupException {	
		try {
			buildRoot = Directory.open(settingsFile.getDirectory().getPath());
			scriptEngine = new GroovyScriptEngine(buildRoot.getPath());
			builder = dbuilder;
			builderDescriptor = loader.getBuilder(settingsFile.getName());
			
			// Wrap settings into simple class declaration
			// to inherit from base builder settings class
			// Also include some imports, so that settings
			// script could use justbuild java api-s.
	        File wrappedSettings = settingsFile.getTmpCopy();
	        try (FileWriter writer = new FileWriter(wrappedSettings.getUnderlyingFile(), false)){
	            writer.write(ScriptWrapper.wrap(settingsFile, builderDescriptor.getSettingsScript().getName()));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        // Load base settings class for inheritance
	        scriptEngine.loadScriptByName(builderDescriptor.getSettingsScript().getPath());
	        // Load all additional classes first as builder
	        // and settings may need them
	        if (builderDescriptor.getOtherFiles()!=null){
		        for (File additionalFile: builderDescriptor.getOtherFiles()) {
		            scriptEngine.loadScriptByName(additionalFile.getPath());
		        }
	        }
	        // Then load settings itself
	        @SuppressWarnings("unchecked")
	        Class<Settings> settingsClass = (Class<Settings>) scriptEngine.loadScriptByName(wrappedSettings.getPath());
	        // Get settings object constructor
			Constructor<Settings> builderConstructor = settingsClass.getDeclaredConstructor(Directory.class);
			// And instantiate settings object
			Object settingsObject = builderConstructor.newInstance(buildRoot);
			this.settings = (Settings) settingsObject;
			try {
				Files.delete(Paths.get(wrappedSettings.getPath()));
			}catch (NoSuchFileException e){
				logger.warning("Could not remove "+wrappedSettings.getPath());
			}
		}catch (IOException 
				|ResourceException
				|ScriptException
				|NoSuchMethodException
				|SecurityException
				|InstantiationException
				|IllegalAccessException
				|IllegalArgumentException
				|InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (BuilderNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
	}
	
   	public BuildJob(File settingsFile) throws BuildSetupException {	
		try {
			buildRoot = Directory.open(settingsFile.getDirectory().getPath());
			scriptEngine = new GroovyScriptEngine(buildRoot.getPath());
			builderDescriptor = loader.getBuilder(settingsFile.getName());
			
			// Wrap settings into simple class declaration
			// to inherit from base builder settings class
			// Also include some imports, so that settings
			// script could use justbuild java api-s.
	        File wrappedSettings = settingsFile.getTmpCopy();
	        try (FileWriter writer = new FileWriter(wrappedSettings.getUnderlyingFile(), false)){
	            writer.write(ScriptWrapper.wrap(settingsFile, builderDescriptor.getSettingsScript().getName()));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        // Load base settings class for inheritance
	        scriptEngine.loadScriptByName(builderDescriptor.getSettingsScript().getPath());
	        // Load all additional classes first as builder
	        // and settings may need them
	        if (builderDescriptor.getOtherFiles()!=null){
		        for (File additionalFile: builderDescriptor.getOtherFiles()) {
		            scriptEngine.loadScriptByName(additionalFile.getPath());
		        }
	        }
	        // Then load settings itself
	        @SuppressWarnings("unchecked")
	        Class<Settings> settingsClass = (Class<Settings>) scriptEngine.loadScriptByName(wrappedSettings.getPath());
	        // Get settings object constructor
			Constructor<Settings> builderConstructor = settingsClass.getDeclaredConstructor(Directory.class);
			// And instantiate settings object
			Object settingsObject = builderConstructor.newInstance(buildRoot);
			this.settings = (Settings) settingsObject;
			try {
				Files.delete(Paths.get(wrappedSettings.getPath()));
			}catch (NoSuchFileException e){
				logger.warning("Could not remove "+wrappedSettings.getPath());
			}
		}catch (IOException 
				|BuilderNotFoundException
				|ResourceException
				|ScriptException
				|NoSuchMethodException
				|SecurityException
				|InstantiationException
				|IllegalAccessException
				|IllegalArgumentException
				|InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 		
	}
	
	
	
	public BuildResult build() throws BuildFailedException
	{
		// Initialize settings
		try {
			settings.initialize();
			final Queue<Command> commands = new LinkedList<Command>();
			final BuildResult result = BuildResult.OK;
			
			if (builder == null) {				
				// Generate commands
				String builderFileName = builderDescriptor.getBuilderScript().getPath();
				@SuppressWarnings("unchecked")
				Class<Builder> builderClass = (Class<Builder>) scriptEngine.loadScriptByName(builderFileName);
				builder = (Builder) builderClass.newInstance();
			}
			builder.config(settings);
			builder.initialize();
			builder.generate();
			Queue<Command> phaseCmds = builder.getCommands();
			result.addArtifacts(builder.getArtifacts());
			commands.addAll(phaseCmds);

			// Run those little fuckers
			while (!commands.isEmpty()) {
				Command cmd = commands.poll();
				System.out.println("$ "+cmd);
				result.appendResult(cmd.run());
				result.flushOut();
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BuildFailedException();
		}
	}
}
