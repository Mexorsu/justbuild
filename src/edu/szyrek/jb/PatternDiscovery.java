package edu.szyrek.jb;

import java.util.ArrayList;
import java.util.List;

import edu.szyrek.jb.api.Discovery;

public abstract class PatternDiscovery implements Discovery {
	List<String> includes = new ArrayList<>();
	List<String> excludes = new ArrayList<>();
	
	public PatternDiscovery(String includePattern, String excludePattern) {
		includes.add(includePattern);
		excludes.add(excludePattern);
	}

	@Override
	public List<String> includes() {
		return includes;
	}

	@Override
	public List<String> excludes() {
		return excludes;
	}

}
