package edu.szyrek.jb;

public class BuilderNotFoundException extends Exception {
	public BuilderNotFoundException(String string) {
		super(string);
	}
	
	private static final long serialVersionUID = 6833812532378159875L;
}