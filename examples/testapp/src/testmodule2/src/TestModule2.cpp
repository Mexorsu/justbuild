#include"TestModule2.h"
#include"TestModule.h"
#include<iostream>

void printHelloFromModule2() {
    std::cout << "Hello world from test module2" << std::endl;
    std::cout << "  value from module1: " << getIntFromModule1() << std::endl;
}
// for circular dependencies test
int getIntFromModule2() {
    return 12;
}
