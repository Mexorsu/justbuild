#include"TestModule.h"
#include"TestModule2.h"
#include<iostream>

void printHelloFromModule1() {
    std::cout << "Hello world from test module" << std::endl;
    std::cout << "  value from module2: " << getIntFromModule2() << std::endl;
}
// for circular dependencies test
int getIntFromModule1() {
    return 7;
}
