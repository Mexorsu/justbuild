class LibWrapper
{
public:
    LibWrapper();
    ~LibWrapper();
    int callLibraryFunction1();
    int callLibraryFunction2(int a, int b);
private:
    /* data */
};
