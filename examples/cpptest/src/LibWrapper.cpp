#include"LibWrapper.h"
#include<LibraryHeader.h>

LibWrapper::LibWrapper()
{
}
LibWrapper::~LibWrapper()
{
}
int LibWrapper::callLibraryFunction1()
{
    LibraryClass wrapped;
    return wrapped.libraryFunction1();
}
int LibWrapper::callLibraryFunction2(int a, int b)
{
    LibraryClass wrapped;
    return wrapped.libraryFunction2(a, b);
}
