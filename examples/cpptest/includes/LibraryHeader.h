class LibraryClass
{
public:
    LibraryClass();
    ~LibraryClass();
    // returns 1
    int libraryFunction1();
    // returns a+b
    int libraryFunction2(int a, int b);
};
