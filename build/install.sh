#!/bin/bash
do_as_su(){
    if [[ $EUID -ne 0 ]]
    then
        if hash sudo >/dev/null 2>&1
        then
            sudo $1
        else
            echo "This script must be run as super user!"
            exit 1
        fi
    else
        $1
    fi
}
INSTALL_TARGET=/opt/justbuild
do_as_su "rm -rf $INSTALL_TARGET"
do_as_su "mkdir -p $INSTALL_TARGET"
do_as_su "cp -f jb.jar $INSTALL_TARGET"
do_as_su "cp -f jb.sh $INSTALL_TARGET"
USER_HOME=$(eval echo ~${SUDO_USER})
CONFIG_DIR="$USER_HOME/.jb"
mkdir -p $CONFIG_DIR
cp -rf ../builders $CONFIG_DIR

if hash update-alternatives >/dev/null 2>&1
then
    do_as_su "update-alternatives --install /usr/bin/jb jb /opt/justbuild/jb.sh 1"
else
    do_as_su "ln -s /opt/justbuild/jb.sh /usr/bin/jb"
fi
