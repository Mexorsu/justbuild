package edu.szyrek.jb;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.szyrek.jb.util.Directory;
import edu.szyrek.jb.util.Path;


public class DirectoryUT {
	static File runtimeDir = new File(System.getProperty("user.dir"));
	static File testDir;
	static File cppDir;
	static void deleteDir(File file) {
		if (file==null) return;
		if (file.isDirectory()) {			
			for (File f: file.listFiles()) {
				deleteDir(f);
			}
		}else{
			try{
				file.delete();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	enum FileType {
		FILE,
		DIRECTORY
	}
	
	@BeforeClass
	public static void setupAll() throws IOException {
		String rootDirPath = runtimeDir.getAbsolutePath();
		File rootDir = new File(rootDirPath);
		
		testDir = new File(rootDir.getAbsolutePath()+File.separator+"test_dir");
		cppDir = new File(testDir.getAbsoluteFile()+File.separator+"cpp_test_dir");
		
		try {
			deleteDir(testDir);
		}catch(Exception e){
			//fuck that
		}
		testDir.mkdirs();
		cppDir.mkdirs();
		
		for (int i = 0; i < 10; i++) {
			File newCppFile = new File(cppDir.getAbsolutePath()+File.separator+"File"+i+".cpp");
			newCppFile.createNewFile();
		}
	}

	@Before
	public void setup() {
		File newFile = new File(testDir.getAbsolutePath()+ File.separator +"new_file");
		File newDir = new File(testDir.getAbsolutePath()+ File.separator + "new_dir");
		if (newFile.exists()){
			newFile.delete();
		}
		if (newDir.exists()){
			newDir.delete();
		}
	}
	
	@Test
	public void testGetPath() throws NotDirectoryException, FileNotFoundException {
		Directory theDir = Directory.open(testDir.getAbsolutePath());
		Assert.assertEquals(testDir.getAbsolutePath(), theDir.getPath());
	}
	
	@Test
	public void testGetDir() throws NotDirectoryException, FileNotFoundException {
		Directory theDir = Directory.open(testDir.getAbsolutePath());
		Assert.assertEquals(testDir, theDir.getFile());
	}
	
	@Test
	public void testFindByName() throws NotDirectoryException, FileNotFoundException {
		Directory theDir = Directory.open(cppDir.getAbsolutePath());
		String regex = "F[iI]le3.*";
		List<edu.szyrek.jb.util.File> files = theDir.findByName(regex);
		Assert.assertEquals(1, files.size());
		edu.szyrek.jb.util.File f = files.get(0);
		Assert.assertEquals("File3.cpp", f.getName());
		files = theDir.findByName(".*\\.cpp");
		Assert.assertEquals(10,files.size());
	}
	
	@Test
	public void testGetFiles() throws IOException {
		Directory theDir = Directory.open(cppDir.getAbsolutePath());
		List<Path> back = theDir.getChildren();
		Assert.assertEquals(10, back.size());
	
	}
	
	@Test(expected = FileNotFoundException.class)
	public void testOpenGarbage() throws NotDirectoryException, FileNotFoundException {
        @SuppressWarnings("unused")
		Directory dir = Directory.open("asdfaklsjdd");
	}
	
	@Test(expected = NotDirectoryException.class)
	public void testOpenFileAsDir() throws NotDirectoryException, FileNotFoundException {
		File file = new File(testDir.getAbsolutePath()+ File.separator +"new_file");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail("Could not create test file!");
		}
        @SuppressWarnings("unused")
		Directory dir = Directory.open(file.getAbsolutePath());
	}
	
	@Test()
    public void testCreationOK() throws FileAlreadyExistsException, NotDirectoryException {
    	try {
			Directory theDir = Directory.create(testDir.getAbsolutePath()+ File.separator + "new_dir");
			Assert.assertEquals(testDir+File.separator+"new_dir", theDir.getPath());
		} catch (FileAlreadyExistsException e) {
			e.printStackTrace();
			Assert.fail("Dir already existed while it shouldn't");
		}
    }
	
	@Test(expected = FileAlreadyExistsException.class)
    public void testDoubleCreation() throws FileAlreadyExistsException, NotDirectoryException {
		// Create dir first time
    	try {
			Directory.create(testDir+ File.separator + "new_dir");
		} catch (FileAlreadyExistsException e) {
			e.printStackTrace();
			Assert.fail("Dir already existed while it shouldn't");
		}
    	// Create second time- exception expected
    	Directory.create(testDir+ File.separator + "new_dir");
    }
}
