package edu.szyrek.jb;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

import edu.szyrek.jb.util.DoubleLinkedList;

public class DoubleLinkedListUT {
	DoubleLinkedList<String> testList;
	
	
	@Before
	public void setup() {
		testList = new DoubleLinkedList<>();
		testList.offer("FIRST");
		testList.offer("THIRD");
		testList.offer("FIFTH");	
	}
	
	@Test
	public void testSetup() {
		String first = testList.poll();
		String third = testList.poll();
		String fifth = testList.poll();
		Assert.assertEquals("FIRST", first);
		Assert.assertEquals("THIRD", third);
		Assert.assertEquals("FIFTH", fifth);
	}
	
	private void assertList() {
		String first = testList.poll();
		String second = testList.poll();
		String third = testList.poll();
		String fourth = testList.poll();
		String fifth = testList.poll();
		Assert.assertEquals("FIRST", first);
		Assert.assertEquals("SECOND", second);
		Assert.assertEquals("THIRD", third);
		Assert.assertEquals("FOURTH", fourth);
		Assert.assertEquals("FIFTH", fifth);
		for (String s: testList){
			System.out.println(s);
		}
	}
	
	@Test
	public void testAddBefore() {
		testList.addBefore("SECOND", "THIRD");
		testList.addBefore("FOURTH", "FIFTH");
		assertList();
	}
	@Test
	public void testAddAfter() {
		testList.addAfter("SECOND", "FIRST");
		testList.addAfter("FOURTH", "THIRD");
		assertList();
	}
}
